# Game-shopping 

## Link: https://volatile-body.surge.sh/

- Game-shopping é um simples projeto Frontend, em eu utilizo o Reactjs. Com isso,
 a partir dessa lista de games, o usuário pode adicionar, alterar e remover os 
 jogos que deseja, além disso também tem a soma dos valores de cada game.

# O que funciona : 
    * Lista com 9 jogos;
    * botão de adicionar cada jogo no carrinho;
    * botão do carrinho com os jogos que o usuário 
    escolher por meio do botão adicionar;
    * Ao adicionar o vários do mesmo item no carrinho o mesmo é agrupado;
    * O usuário pode adicionar e remover ou alterar a quantidade de produtos do carrinho;
    * Os valores monetários devem ser formatados de acordo com a moeda brasileira;

# O que não funciona :
    * Subtotal;
    * Feedbacks visuais das ações do usuários (loading em botões, mensagens de sucesso em toast...);
  

